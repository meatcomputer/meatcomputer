+++
date = "2021-3-09T13:49:44+04:00"
draft = false
title = "Contact"
slug = "contact"

+++
  
Mastodon: @meatcomputer@mastodon.social

Youtube: https://www.youtube.com/channel/UCvEmTyhMnoNT8RHgA3Ycq8A

Instagram: @fleshcomputer

<div class="py2">
  <form action="//formspree.io/meatcomputer@protonmail.com" method="POST" class="form-stacked form-light">
    <input type="text" name="email" class="input mobile-block" placeholder="Email Address">
    <textarea type="text" name="content" class="input mobile-block" rows="5" placeholder="What would you like to say?"></textarea>
    <input type="submit" class="button button-blue button-big mobile-block" value="Send">
  </form>
</div>
