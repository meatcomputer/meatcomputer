---
title: "Anonymous Box Interactive Sculpture"
date: 2020-02-27T14:37:18-05:00
tags: ["Art", "Projects", "Electronics", "Arduino", "Welding", "old"]
draft: false
---

![](/img/Projects/anonymousbox.jpg)
This is an old project I made in 2013 at Hampshire College. The project has it's own website https://anonymousbox.github.io where you can read about the project as well as look at documentation of the building process. 
