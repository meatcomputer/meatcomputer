---
title: "Restoring the Original Gameboy"
date: 2021-07-20T15:05:19-04:00
tags: ["electronics", "projects"]
draft: false
---
{{< youtube 0-Prl6LfYds >}}

Found an original gameboy at the local dump. Restored it on video with some smooth jazz background music. 
