---
title: "Gas Support Lift"
date: 2020-01-12T21:32:41-05:00
draft: false
tags: ["Shack", "Project"]

---
![](/img/Shack/GasSupportLifts.jpg)
I made a large door that opens vertically upwards. 
The door is made of a 2x4 frame covered with the old steel roofing panels. I wanted to add gas support lifts, the same supports that support back door of a car or suv. 
I had to fully extend the door to get enough leverage to install and compress the lifts.
