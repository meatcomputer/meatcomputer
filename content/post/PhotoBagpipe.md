---
title: "PhotoBagpipe Demo"
date: 2020-02-18T19:52:12-05:00
tags: ["project", "video", "electronics", "music"]
draft: false
---
![](/img/Projects/photobagpipe.jpg)

I made this project using an old altoids tin with a salt water electro-etched faceplate and the Mozzi library added to a Teensy 3.2. 
The panelmount micro usb outlet was created by my friend at: 

https://www.tall-dog.com/ 

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://diode.zone/videos/embed/2d54fc06-2bb3-46cc-8ee2-32b5cea45094?warningTitle=0" frameborder="0" allowfullscreen></iframe>

Video: https://diode.zone/videos/watch/2d54fc06-2bb3-46cc-8ee2-32b5cea45094