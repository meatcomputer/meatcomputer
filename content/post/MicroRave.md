---
title: "25 Minutes of the MicroRave"
date: 2021-03-16T15:05:19-04:00
tags: ["art", "sculpture", "electronics", "projects"]
draft: false
---
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://diode.zone/videos/embed/61828d57-d2a8-46ac-8cdc-17d12d414db0" frameborder="0" allowfullscreen></iframe>

I modified a microwave, removing all heating related components and replacing them with a light show and speakers. The microrave has an auxillary input in the back so it can be supplied with music from a variety of sources. 
