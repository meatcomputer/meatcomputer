---
title: "Ringing Bell Project and Tutorial"
date: 2020-02-21T12:00:24-05:00
tags: ["electronics","arduino","project","video","tutorial"]
draft: false
---

![](/img/Projects/ringrdemo.png)

I made this solenoid bell ringing project about a year ago now. It is a custom made controller box which controls the solenoid's movements and a custom made solenoid made from a nail, spring, pen barrel, and enamel coated wire.
	
Watch here: https://diode.zone/videos/watch/e5a7505e-3f9e-4e13-9b1c-820c65b2f46b