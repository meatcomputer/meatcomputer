---
title: "Map Adventure Frame"
date: 2020-01-12T17:16:35-05:00
draft: false
tags: ["programming", "Raspberry Pi", "Project"]
---

Map Adventure Frame is small website which allows users to display different maps on a display. 
The system uses a raspberrypi with flask and open street maps. 
