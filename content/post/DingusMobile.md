---
title: "DingusMobile Salvaged GoKart"
date: 2021-05-16T15:05:19-04:00
tags: ["art", "projects", "sculpture", "electronics", "projects", "vehicle"]
draft: false
---
![](/img/Projects/DingusMobile/mobile.jpg)
I made this GoKart using almost entirely salvaged components.

Parts
* Electrical Box
* 250w scooter motor
* 3D Printed Dewalt Battery Connectors
* 25h Chain
* Scooter wheel

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Dingus Mobile Test Run" src="https://diode.zone/videos/embed/8e6df876-82bf-4ae0-8478-f560aa15d5f3" frameborder="0" allowfullscreen></iframe>
