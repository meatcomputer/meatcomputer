---
title: "The Best Time To Use A 3D dPrinter"
date: 2020-01-20T14:34:48-05:00
tags: ['article', 'blog']
draft: false
---

![](/img/OpenScad.png)

I have been on the peripherals of 3D Printing Technology since 2012. Mostly as an observer but as an active user for the last 3 years or so. As an observer I have noticed a litany of pitfalls that users fall in when trying to utilize a 3D printer. This list is almost exclusively about FDM 3D Printers. 

I Hope this brief list can uplift new and experienced 3D Printing users. 


### When you need a crappy plastic part.

  3D printer's  oftentimes lure users into believing that anything is possible. Afterall the technology is rather futuristic and magical. Creating an infinite number plastic parts from a very small GCODE file. It is useful to remember that 3D printer's are near perfect machines when it comes to making low quality plastic parts, but other than that you may well enounter difficulties while using the printer. 
  Sometimes it is incredibly useful to have crappy plastic parts, for prototyping, as a part of a project that experiences very little stresses, or as a mold to cast durable objects from. 
  Keeping this notion in mind 3D printers can be used to print durable, load bearing or long lasting projects but anytime you want to print anything other than a low quality plastic part you should put more thought into the design of the part and configuation of the printer.
  
### As a Crutch

  It can be difficult to fabricate presice parts. Especially when you need them in a large quantity or on a deadline or if you didn't get much sleep the night before. 3D printers are a good crutch for anyone who lacks precision fabrication skills.So long as you pay close attention to the measurements and have a properly calibrated printer you can create panels with precise holes and slots for switches and inputs.
  
  The trick with using a 3D printer as a crutch is that you must recognize thatyou are usingthe 3D printer as a crutch. If you aknowledge that you are using the 3D printer as a crutch you can think more critically about your designs and ask yourself the question "Does that part really need to be 3D printed or am I just using the printer as a crutch?" 
  
  
