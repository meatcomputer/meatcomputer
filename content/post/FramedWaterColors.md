---
title: "Framed Water Colors"
date: 2020-02-07T12:36:35-05:00
tags: ['Drawing', 'Art']
draft: false
---

Don't normally work in color, or put frames on my illustrations but I did for these about 2 years ago.
![](/img/Drawings/FramedPiece1.jpg)

![](/img/Drawings/FramedPiece2.jpg)

![](/img/Drawings/FramedPiece3.jpg)
