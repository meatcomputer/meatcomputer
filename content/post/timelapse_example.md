---
title: "Timelapse Example Video"
date: 2020-06-16T15:05:19-04:00
tags: ["timelapse", "android", "repurposing", "projects"]
draft: false
---

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://diode.zone/videos/embed/05bbcc0e-7e9f-4e18-bfd3-c76072b88c2f" frameborder="0" allowfullscreen></iframe>

This is the first week long test of the timelapse unit. The android phone that is taking the pictures powered off after 5 days instead of staying on the whole time. The battery pack was at half charge when I checked the unit so either the battery ran out, phone died and then the solar panel charged the pack back to half full by the time I checked it or the phone may have shut off due to heat. 

For the next test I am going to use a phone with a better internal battery and setup another unit with slots made in the lower section of the case to see if the phone that only lasted for 5 days will last for longer with the increased airflow. 