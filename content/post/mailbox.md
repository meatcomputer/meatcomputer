---
title: "Large Welded Mailbox"
date: 2021-05-14T18:04:03-05:00
tags: ["Metalworking", "Welding", "Projects"]
draft: false
---
I made this mailbox out of plate steel from an old factory building.
![](/img/Projects/mailbox/mailboxfinweb.jpg)
Welded with 6011 Rods, the surface was then lightly sanded and finished with a copper sulfate patina. I painted epoxy on the mailbox to prevent the copper and steel from rusting. The door is made of wood with captive magnets on the top edge as a latching mechanism.  
![](/img/Projects/mailbox/patinapicture.jpg)
![](/img/Projects/mailbox/mailboxinprocessweb.jpg)
