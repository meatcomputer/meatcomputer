---
title: "Timelapse_prototype"
date: 2020-06-11T12:04:09-04:00
tags: ["Projects", "Electronics", "Timelapse"]
draft: false
---

![](/img/timelapse/FirstPrototype.jpg)
This is the first prototype of a timelapse photography setup I made to help to local conservationists look at plant growth over the course of the year. 

The prototype uses old android phones to take one picture per hour of daylight. There is a battery bank and a solar panel connected to the battery bank to ensure that the phone can function for months (maybe years) at a time without needing service.  

